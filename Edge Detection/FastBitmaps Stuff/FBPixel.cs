﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.FastBitmaps_Stuff
{
    public class FBPixel
    {
        public byte[] Data;

        public byte A
        {
            get { return Data[3]; }
            set { Data[3] = value; }
        }
        public byte R
        {
            get { return Data[2]; }
            set { Data[2] = value; }
        }
        public byte G
        {
            get { return Data[1]; }
            set { Data[1] = value; }
        }
        public byte B
        {
            get { return Data[0]; }
            set { Data[0] = value; }
        }


        public FBPixel(byte intensity)
        {
            Data = new byte[4];

            A = 255;
            R = intensity;
            G = intensity;
            B = intensity;
        }
        public FBPixel(byte r, byte g, byte b)
        {
            Data = new byte[4];

            A = 255;
            R = r;
            G = g;
            B = b;
        }
        public FBPixel(byte a, byte r, byte g, byte b)
        {
            Data = new byte[4];

            A = a;
            R = r;
            G = g;
            B = b;
        }
        public FBPixel(FBPixel fbPixel)
        {
            Data = new byte[4];

            for (int i = 0; i < Data.Length; i++)
                Data[i] = fbPixel.Data[i];
        }
        public FBPixel(Color color)
        {
            Data = new byte[4];

            A = color.A;
            R = color.R;
            G = color.G;
            B = color.B;
        }


        public void Set(byte intensity)
        {
            A = 255;
            R = intensity;
            G = intensity;
            B = intensity;
        }
        public void Set(byte r, byte g, byte b)
        {
            A = 255;
            R = r;
            G = g;
            B = b;
        }
        public void Set(byte a, byte r, byte g, byte b)
        {
            A = a;
            R = r;
            G = g;
            B = b;
        }

        public void SetColor(Color color)
        {
            A = color.A;
            R = color.R;
            G = color.G;
            B = color.B;
        }
        public Color GetColor()
        {
            return Color.FromArgb(A, R, G, B);
        }
    }
}
