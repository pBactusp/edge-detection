﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace Edge_Detection.FastBitmaps_Stuff
{
    public class FastBmp
    {
        private FBPixel[,] fbPixels;

        public int Width { get; }
        public int Height { get; }

        public FBPixel this[int x, int y]
        {
            get { return fbPixels[x, y]; }
            set { fbPixels[x, y] = value; }
        }

        public FastBmp(Size size, Color DefaultColor)
        {
            Width = size.Width;
            Height = size.Height;

            fbPixels = new FBPixel[Width, Height];

            byte a = DefaultColor.A;
            byte r = DefaultColor.R;
            byte g = DefaultColor.G;
            byte b = DefaultColor.B;

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    fbPixels[x, y] = new FBPixel(a, r, g, b);
        }
        public FastBmp(int width, int height, Color DefaultColor)
        {
            Width = width;
            Height = height;

            fbPixels = new FBPixel[Width, Height];

            byte a = DefaultColor.A;
            byte r = DefaultColor.R;
            byte g = DefaultColor.G;
            byte b = DefaultColor.B;

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    fbPixels[x, y] = new FBPixel(a, r, g, b);
        }
        public FastBmp(byte[,] source)
        {
            Width = source.GetLength(0);
            Height = source.GetLength(1);

            fbPixels = new FBPixel[Width, Height];

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    fbPixels[x, y] = new FBPixel(source[x, y]);
        }
        public FastBmp(float[,] source)
        {

            Width = source.GetLength(0);
            Height = source.GetLength(1);

            float[,] sourceNormallized = new float[Width, Height];

            float min = source[0, 0];
            float max = source[0, 0];

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                {
                    min = Math.Min(min, source[x, y]);
                    max = Math.Max(max, source[x, y]);
                }

            fbPixels = new FBPixel[Width, Height];

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    fbPixels[x, y] = new FBPixel((byte)(255f * (source[x, y] - min) / (max - min)));
        }


        public FastBmp(FastBmp source)
        {
            Width = source.Width;
            Height = source.Height;

            fbPixels = new FBPixel[Width, Height];

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    fbPixels[x, y] = new FBPixel(source[x, y]);
        }
        public unsafe FastBmp(Bitmap source)
        {
            Width = source.Width;
            Height = source.Height;

            fbPixels = new FBPixel[Width, Height];

            int bytesPerPixel = Bitmap.GetPixelFormatSize(source.PixelFormat) / 8;
            BitmapData sourceData = source.LockBits(new Rectangle(new Point(0, 0), source.Size), ImageLockMode.ReadOnly, source.PixelFormat);
            int pixelsWidth = sourceData.Width * bytesPerPixel;
            byte* ptrToSource = (byte*)sourceData.Scan0;
            int indexInSource = 0;

            for (int y = 0; y < Height; y++)
            {
                indexInSource = y * sourceData.Stride;

                for (int x = 0; x < Width; x++)
                {
                    switch (bytesPerPixel)
                    {
                        case 1:
                            fbPixels[x, y] = new FBPixel(ptrToSource[indexInSource]);
                            break;
                        case 3:
                            fbPixels[x, y] = new FBPixel(255, ptrToSource[indexInSource + 2], ptrToSource[indexInSource + 1], ptrToSource[indexInSource]);
                            break;
                        case 4:
                            fbPixels[x, y] = new FBPixel(ptrToSource[indexInSource+ 3], ptrToSource[indexInSource + 2], ptrToSource[indexInSource + 1], ptrToSource[indexInSource]);
                            break;

                        default:
                            throw new Exception($"PixelFormat of length {bytesPerPixel} was not recognized.");
                    }

                    indexInSource += bytesPerPixel;
                }
            }

            source.UnlockBits(sourceData);
        }

        public unsafe Bitmap RenderToBitmap()
        {
            Bitmap ret = new Bitmap(Width, Height);

            BitmapData retData = ret.LockBits(new Rectangle(new Point(0, 0), ret.Size), ImageLockMode.WriteOnly, ret.PixelFormat);

            byte* ptrToSource = (byte*)retData.Scan0;
            int indexInSource = 0;

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    for (int i = 0; i < 4; i++)
                        ptrToSource[indexInSource + i] = fbPixels[x, y].Data[i];

                    indexInSource += 4;
                }
            }

            ret.UnlockBits(retData);

            return ret;
        }


        //private void ProcessUsingLockbits(Bitmap processedBitmap)
        //{
        //    BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

        //    int bytesPerPixel = Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
        //    int byteCount = bitmapData.Stride * processedBitmap.Height;
        //    byte[] pixels = new byte[byteCount];
        //    IntPtr ptrFirstPixel = bitmapData.Scan0;
        //    Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);

        //    int heightInPixels = bitmapData.Height;
        //    int widthInBytes = bitmapData.Width * bytesPerPixel;

        //    for (int y = 0; y < heightInPixels; y++)
        //    {
        //        int currentLine = y * bitmapData.Stride;
        //        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
        //        {
        //            int oldBlue = pixels[currentLine + x];
        //            int oldGreen = pixels[currentLine + x + 1];
        //            int oldRed = pixels[currentLine + x + 2];

        //            calculate new pixel value
        //           pixels[currentLine + x] = (byte)oldBlue;
        //            pixels[currentLine + x + 1] = (byte)oldGreen;
        //            pixels[currentLine + x + 2] = (byte)oldRed;
        //        }
        //    }

        //    copy modified bytes back
        //    Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);
        //    processedBitmap.UnlockBits(bitmapData);
        //}




        //private void ProcessUsingLockbitsAndUnsafe(Bitmap processedBitmap)
        //{
        //    unsafe
        //    {
        //        BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);
        //        int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
        //        int heightInPixels = bitmapData.Height;
        //        int widthInBytes = bitmapData.Width * bytesPerPixel;
        //        byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

        //        for (int y = 0; y < heightInPixels; y++)
        //        {
        //            byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
        //            for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
        //            {
        //                int oldBlue = currentLine[x];
        //                int oldGreen = currentLine[x + 1];
        //                int oldRed = currentLine[x + 2];

        //                calculate new pixel value
        //               currentLine[x] = (byte)oldBlue;
        //                currentLine[x + 1] = (byte)oldGreen;
        //                currentLine[x + 2] = (byte)oldRed;
        //            }
        //        }
        //        processedBitmap.UnlockBits(bitmapData);
        //    }
        //}




        //private void ProcessUsingLockbitsAndUnsafeAndParallel(Bitmap processedBitmap)
        //{
        //    unsafe
        //    {
        //        BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

        //        int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
        //        int heightInPixels = bitmapData.Height;
        //        int widthInBytes = bitmapData.Width * bytesPerPixel;
        //        byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

        //        Parallel.For(0, heightInPixels, y =>
        //        {
        //            byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
        //            for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
        //            {
        //                int oldBlue = currentLine[x];
        //                int oldGreen = currentLine[x + 1];
        //                int oldRed = currentLine[x + 2];

        //                currentLine[x] = (byte)oldBlue;
        //                currentLine[x + 1] = (byte)oldGreen;
        //                currentLine[x + 2] = (byte)oldRed;
        //            }
        //        });
        //        processedBitmap.UnlockBits(bitmapData);
        //    }
        //}




    }
}
