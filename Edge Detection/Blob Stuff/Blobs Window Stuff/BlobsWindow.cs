﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Edge_Detection.Blob_Stuff
{
    public partial class BlobsWindow : Form
    {
        public BlobsDataset BlobsDataset;
        
        private int minBlobSize { get { return (int)minBlobSizeNUPDO.Value; } }
        private int maxBlobSize { get { return maxBlobSizeNUPDO.Enabled ? (int)maxBlobSizeNUPDO.Value : -1; } }


        public BlobsWindow(BlobsDataset blobsDataset)
        {
            InitializeComponent();

            BlobsDataset = blobsDataset;
            
            General.SetPanelDoubleBuffered(outputPA, true);

            BlobsSizesData bsd = BlobsDataset.Blobs.GetBlobsSizesData();

            minBlobSizeNUPDO.Minimum = bsd.SizeOfSmallestBlob;
            minBlobSizeNUPDO.Maximum = bsd.SizeOfLargestBlob;
            minBlobSizeNUPDO.Value = minBlobSizeNUPDO.Minimum;

            maxBlobSizeNUPDO.Minimum = minBlobSizeNUPDO.Value;
            maxBlobSizeNUPDO.Maximum = bsd.SizeOfLargestBlob;
            maxBlobSizeNUPDO.Value = maxBlobSizeNUPDO.Maximum;

            avarageBlobSizeLA.Text = bsd.AvarageSizeOfBlobs.ToString();
        }


        public void RenderBlobs()
        {
            outputPA.BackgroundImage = BlobsDataset.Render(minBlobSize, maxBlobSize, showOriginalImageCHEBO.Checked, true);
        }


        private void BlobsWindow_Shown(object sender, EventArgs e)
        {
            blobNumLA.Text = BlobsDataset.Blobs.Count(blob => blob.Region.Count >= minBlobSize && (blob.Region.Count <= maxBlobSize || maxBlobSize == -1)).ToString();
            RenderBlobs();
        }
        private void useMaxBlobSizeCHEBO_CheckedChanged(object sender, EventArgs e)
        {
            maxBlobSizeNUPDO.Enabled = useMaxBlobSizeCHEBO.Checked;
            blobNumLA.Text = BlobsDataset.Blobs.Count(blob => blob.Region.Count >= minBlobSize && (blob.Region.Count <= maxBlobSize || maxBlobSize == -1)).ToString();
            RenderBlobs();
        }
        private void minBlobSizeNUPDO_ValueChanged(object sender, EventArgs e)
        {
            maxBlobSizeNUPDO.Minimum = minBlobSizeNUPDO.Value;
            blobNumLA.Text = BlobsDataset.Blobs.Count(blob => blob.Region.Count >= minBlobSize && (blob.Region.Count <= maxBlobSize || maxBlobSize == -1)).ToString();
            RenderBlobs();
        }
        private void maxBlobSizeNUPDO_ValueChanged(object sender, EventArgs e)
        {
            minBlobSizeNUPDO.Maximum = maxBlobSizeNUPDO.Value;
            blobNumLA.Text = BlobsDataset.Blobs.Count(blob => blob.Region.Count >= minBlobSize && (blob.Region.Count <= maxBlobSize || maxBlobSize == -1)).ToString();
            RenderBlobs();
        }
        private void showOriginalImageCHEBO_CheckedChanged(object sender, EventArgs e)
        {
            RenderBlobs();
        }

        private void saveBU_Click(object sender, EventArgs e)
        {
            outputPA.BackgroundImage.Save(@"D:\Pictures\KKKizel.jpg");
        }
    }
}
