﻿namespace Edge_Detection.Blob_Stuff
{
    partial class BlobsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputPA = new System.Windows.Forms.Panel();
            this.minBlobSizeNUPDO = new System.Windows.Forms.NumericUpDown();
            this.maxBlobSizeNUPDO = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.useMaxBlobSizeCHEBO = new System.Windows.Forms.CheckBox();
            this.avarageBlobSizeLA = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.blobNumLA = new System.Windows.Forms.Label();
            this.showOriginalImageCHEBO = new System.Windows.Forms.CheckBox();
            this.saveBU = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.minBlobSizeNUPDO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxBlobSizeNUPDO)).BeginInit();
            this.SuspendLayout();
            // 
            // outputPA
            // 
            this.outputPA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputPA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.outputPA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outputPA.Location = new System.Drawing.Point(12, 63);
            this.outputPA.Name = "outputPA";
            this.outputPA.Size = new System.Drawing.Size(776, 375);
            this.outputPA.TabIndex = 0;
            // 
            // minBlobSizeNUPDO
            // 
            this.minBlobSizeNUPDO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.minBlobSizeNUPDO.Location = new System.Drawing.Point(712, 7);
            this.minBlobSizeNUPDO.Name = "minBlobSizeNUPDO";
            this.minBlobSizeNUPDO.Size = new System.Drawing.Size(76, 22);
            this.minBlobSizeNUPDO.TabIndex = 1;
            this.minBlobSizeNUPDO.ThousandsSeparator = true;
            this.minBlobSizeNUPDO.ValueChanged += new System.EventHandler(this.minBlobSizeNUPDO_ValueChanged);
            // 
            // maxBlobSizeNUPDO
            // 
            this.maxBlobSizeNUPDO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maxBlobSizeNUPDO.Enabled = false;
            this.maxBlobSizeNUPDO.Location = new System.Drawing.Point(712, 35);
            this.maxBlobSizeNUPDO.Name = "maxBlobSizeNUPDO";
            this.maxBlobSizeNUPDO.Size = new System.Drawing.Size(76, 22);
            this.maxBlobSizeNUPDO.TabIndex = 2;
            this.maxBlobSizeNUPDO.ThousandsSeparator = true;
            this.maxBlobSizeNUPDO.ValueChanged += new System.EventHandler(this.maxBlobSizeNUPDO_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(613, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Min Blob Size";
            // 
            // useMaxBlobSizeCHEBO
            // 
            this.useMaxBlobSizeCHEBO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.useMaxBlobSizeCHEBO.AutoSize = true;
            this.useMaxBlobSizeCHEBO.Location = new System.Drawing.Point(588, 36);
            this.useMaxBlobSizeCHEBO.Name = "useMaxBlobSizeCHEBO";
            this.useMaxBlobSizeCHEBO.Size = new System.Drawing.Size(118, 21);
            this.useMaxBlobSizeCHEBO.TabIndex = 4;
            this.useMaxBlobSizeCHEBO.Text = "Max Blob Size";
            this.useMaxBlobSizeCHEBO.UseVisualStyleBackColor = true;
            this.useMaxBlobSizeCHEBO.CheckedChanged += new System.EventHandler(this.useMaxBlobSizeCHEBO_CheckedChanged);
            // 
            // avarageBlobSizeLA
            // 
            this.avarageBlobSizeLA.AutoSize = true;
            this.avarageBlobSizeLA.Location = new System.Drawing.Point(146, 9);
            this.avarageBlobSizeLA.Name = "avarageBlobSizeLA";
            this.avarageBlobSizeLA.Size = new System.Drawing.Size(16, 17);
            this.avarageBlobSizeLA.TabIndex = 5;
            this.avarageBlobSizeLA.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Avarage Blob Size:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Num of Blobs:";
            // 
            // blobNumLA
            // 
            this.blobNumLA.AutoSize = true;
            this.blobNumLA.Location = new System.Drawing.Point(114, 35);
            this.blobNumLA.Name = "blobNumLA";
            this.blobNumLA.Size = new System.Drawing.Size(16, 17);
            this.blobNumLA.TabIndex = 8;
            this.blobNumLA.Text = "0";
            // 
            // showOriginalImageCHEBO
            // 
            this.showOriginalImageCHEBO.AutoSize = true;
            this.showOriginalImageCHEBO.Checked = true;
            this.showOriginalImageCHEBO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showOriginalImageCHEBO.Location = new System.Drawing.Point(220, 8);
            this.showOriginalImageCHEBO.Name = "showOriginalImageCHEBO";
            this.showOriginalImageCHEBO.Size = new System.Drawing.Size(159, 21);
            this.showOriginalImageCHEBO.TabIndex = 9;
            this.showOriginalImageCHEBO.Text = "Show Original Image";
            this.showOriginalImageCHEBO.UseVisualStyleBackColor = true;
            this.showOriginalImageCHEBO.CheckedChanged += new System.EventHandler(this.showOriginalImageCHEBO_CheckedChanged);
            // 
            // saveBU
            // 
            this.saveBU.Location = new System.Drawing.Point(439, 13);
            this.saveBU.Name = "saveBU";
            this.saveBU.Size = new System.Drawing.Size(75, 23);
            this.saveBU.TabIndex = 10;
            this.saveBU.Text = "save";
            this.saveBU.UseVisualStyleBackColor = true;
            this.saveBU.Click += new System.EventHandler(this.saveBU_Click);
            // 
            // BlobsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.saveBU);
            this.Controls.Add(this.showOriginalImageCHEBO);
            this.Controls.Add(this.blobNumLA);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.avarageBlobSizeLA);
            this.Controls.Add(this.useMaxBlobSizeCHEBO);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.maxBlobSizeNUPDO);
            this.Controls.Add(this.minBlobSizeNUPDO);
            this.Controls.Add(this.outputPA);
            this.DoubleBuffered = true;
            this.Name = "BlobsWindow";
            this.Text = "BlobsWindow";
            this.Shown += new System.EventHandler(this.BlobsWindow_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.minBlobSizeNUPDO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxBlobSizeNUPDO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel outputPA;
        private System.Windows.Forms.NumericUpDown minBlobSizeNUPDO;
        private System.Windows.Forms.NumericUpDown maxBlobSizeNUPDO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox useMaxBlobSizeCHEBO;
        private System.Windows.Forms.Label avarageBlobSizeLA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label blobNumLA;
        private System.Windows.Forms.CheckBox showOriginalImageCHEBO;
        private System.Windows.Forms.Button saveBU;
    }
}