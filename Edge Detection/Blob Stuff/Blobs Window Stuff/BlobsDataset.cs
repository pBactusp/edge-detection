﻿using Edge_Detection.FastBitmaps_Stuff;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class BlobsDataset
    {
        public BlobMask BlobMask;
        public BlobsCollection Blobs;


        private FastBmp original;
        private Dictionary<int, Color> blobColors;


        public Color DefaultBlobColor { get; set; }

        public BlobsDataset(float[,] originalGS, BlobMask blobMask, BlobsCollection blobs)
        {
            DefaultBlobColor = Color.Black;
            this.original = new FastBmp(originalGS);
            BlobMask = blobMask;
            Blobs = blobs;
            blobColors = new Dictionary<int, Color>();

            Random rnd = new Random();

            foreach (Blob blob in Blobs)
                blobColors.Add(blob.Region.Label.Index, Color.FromArgb(255, rnd.Next(256), rnd.Next(256), rnd.Next(256)));
        }
        public BlobsDataset(FastBmp original, BlobMask blobMask, BlobsCollection blobs)
        {
            DefaultBlobColor = Color.Black;
            this.original = new FastBmp(original);
            BlobMask = blobMask;
            Blobs = blobs;
            blobColors = new Dictionary<int, Color>();

            Random rnd = new Random();

            foreach (Blob blob in Blobs)
                blobColors.Add(blob.Region.Label.Index, Color.FromArgb(255, rnd.Next(256), rnd.Next(256), rnd.Next(256)));
        }

        public Bitmap Render(int minBlobSize, int maxBlobSize, bool withOriginal, bool useDifferentColors)
        {
            FastBmp fbmp;

            if (withOriginal)
                fbmp = new FastBmp(original);
            else
                fbmp = new FastBmp(original.Width, original.Height, Color.FromArgb(0, 0, 0, 0));

            BlobsCollection fiteredBlobs = Blobs.GetFiltered_BySize(minBlobSize, maxBlobSize);

            int[,] mask = fiteredBlobs.GetAsMask(original.Width, original.Height);

            for (int y = 0; y < original.Height; y++)
                for (int x = 0; x < original.Width; x++)
                    if (mask[x, y] > 0)
                    {
                        if (useDifferentColors)
                            fbmp[x, y].SetColor(blobColors[mask[x, y]]);
                        else
                            fbmp[x, y].SetColor(DefaultBlobColor);
                    }

            return fbmp.RenderToBitmap();
        }

    }
}
