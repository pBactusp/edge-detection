﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public struct BlobsSizesData
    {
        public int SizeOfSmallestBlob { get; }
        public int SizeOfLargestBlob { get; }
        public float AvarageSizeOfBlobs { get; }


        public BlobsSizesData(int sizeOfSmallestBlob, int sizeOfLargestBlob, float avarageSizeOfBlobs)
        {
            SizeOfSmallestBlob = sizeOfSmallestBlob;
            SizeOfLargestBlob = sizeOfLargestBlob;
            AvarageSizeOfBlobs = avarageSizeOfBlobs;
        }
    }
}
