﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class RLCoord
    {
        public int Length { get; set; }

        public int Row { get; }
        public int Column { get; }


        public RLCoord(int column, int row)
        {
            Column = column;
            Row = row;
            Length = 0;
        }
    }
}
