﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class RegionLabel
    {
        public int Index { get; private set; }
        public int Count { get; private set; }

        public BoundingBox BoundingBox { get; private set; }


        public RegionLabel(int index, int firstPointX, int firstPointY)
        {
            Index = index;
            Count = 1;
            BoundingBox = new BoundingBox(firstPointX, firstPointX, firstPointY, firstPointY);
        }
        public RegionLabel(int index, Point firstPoint)
        {
            Index = index;
            Count = 1;
            BoundingBox = new BoundingBox(firstPoint.X, firstPoint.X, firstPoint.Y, firstPoint.Y);
        }

        public void AddPoint(int x, int y)
        {
            BoundingBox.AddPoint(x, y);
            Count++;
        }
        public void AddPoint(Point point)
        {
            BoundingBox.AddPoint(point);
            Count++;
        }

        public Blob GetBlob(int[,] mask)
        {
            List<RLCoord> rLCoords = new List<RLCoord>();

            for (int y = BoundingBox.Top; y <= BoundingBox.Bottom; y++)
                for (int x = BoundingBox.Left; x <= BoundingBox.Right; x++)
                    if (mask[x, y] == Index)
                    {
                        RLCoord rlc = new RLCoord(x, y);

                        while (x <= BoundingBox.Right && mask[x, y] == Index)
                        {
                            rlc.Length++;
                            x++;
                        }

                        rLCoords.Add(rlc);
                    }

            Region region = new Region(this, rLCoords);

            return new Blob(region);
        }
    }
}
