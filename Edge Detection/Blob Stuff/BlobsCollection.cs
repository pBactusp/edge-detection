﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class BlobsCollection : List<Blob>
    {

        public BlobsCollection()
        {

        }
        public BlobsCollection(IEnumerable<Blob> blobs)
        {
            AddRange(blobs);
        }

        

        public BoundingBox GetBoundingBox()
        {
            if (Count == 0)
                return null;

            BoundingBox boundingBox = new BoundingBox(this[0].BoundingBox);

            for (int i = 1; i < Count; i++)
                boundingBox.AddArea(this[i].BoundingBox);

            return boundingBox;
        }

        public BlobsSizesData GetBlobsSizesData()
        {
            float count = Count;

            if (count == 0)
                return new BlobsSizesData(0, 0, 0);

            int min, max;
            float avrg;

            int currentBlobSize = this[0].Region.Count;

            min = currentBlobSize;
            max = currentBlobSize;

            avrg = (float)currentBlobSize;

            for (int i = 1; i < count; i++)
            {
                currentBlobSize = this[i].Region.Count;

                min = Math.Min(min, currentBlobSize);
                max = Math.Max(max, currentBlobSize);

                avrg += (float)currentBlobSize;
            }

            avrg /= count;

            return new BlobsSizesData(min, max, avrg);
        }

        public int[,] GetAsMask(int width, int height)
        {
            int[,] mask = new int[width, height];

            foreach (Blob blob in this)
                    Blob.RenderToMask(blob, mask);

            return mask;
        }


        /// <summary>
        /// Returns a new BlobsCollection that contains only
        /// blobs which sizes are in the specified range.
        /// </summary>
        /// <param name="minBlobSize">Specifies the smallest a blob can be</param>
        /// <param name="maxBlobSize">Specifies the largest a blob can be (-1 for infinity)</param>
        /// <returns></returns>
        public BlobsCollection GetFiltered_BySize(int minBlobSize, int maxBlobSize)
        {
            BlobsCollection ret = new BlobsCollection();

            foreach (Blob blob in this)
            {
                if (blob.Region.Count >= minBlobSize && (blob.Region.Count <= maxBlobSize || maxBlobSize == -1))
                    ret.Add(blob);
            }

            return ret;

            //return new BlobsCollection(this.Where(blob => blob.Region.Count >= minBlobSize && (blob.Region.Count <= maxBlobSize || maxBlobSize == -1)));
        }
    }
}
