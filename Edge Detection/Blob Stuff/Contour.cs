﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class Contour
    {
        public BoundingBox BoundingBox;
        public Point[] Points;

        public Contour(IEnumerable<Point> points)
        {
            Points = points.ToArray();
            BoundingBox = GetBoundingBox(ref Points);
        }


        private static BoundingBox GetBoundingBox(ref Point[] points)
        {
            if (points.Length < 1)
                return null;

            int left = points[0].X;
            int right = points[0].X;
            int top = points[0].Y;
            int bottom = points[0].Y;

            for (int i = 1; i < points.Length; i++)
            {
                left = Math.Min(left, points[i].X);
                right = Math.Max(right, points[i].X);
                top = Math.Min(top, points[i].Y);
                bottom = Math.Max(bottom, points[i].Y);
            }

            return new BoundingBox(left, right, top, bottom);
        }
    }
}
