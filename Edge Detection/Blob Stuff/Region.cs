﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class Region
    {
        public RLCoord[] RunLengthCoordinates { get; }

        public RegionLabel Label { get; }

        public int Count { get { return Label.Count; } }
        public BoundingBox BoundingBox { get { return Label.BoundingBox; } }
        

        public Region(RegionLabel regionLabel, IEnumerable<RLCoord> runLengthCoordinates)
        {
            Label = regionLabel;
            RunLengthCoordinates = runLengthCoordinates.ToArray();
            Array.Sort(RunLengthCoordinates, new Comparison<RLCoord>((x, y) => x.Row.CompareTo(y.Row)));
        }


        
    }
}
