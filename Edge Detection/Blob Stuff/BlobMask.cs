﻿using Edge_Detection.FastBitmaps_Stuff;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class BlobMask
    {
        public int[,] Mask;
        public List<RegionLabel> RegionLabels;

        public int Width { get { return Mask.GetLength(0); } }
        public int Height { get { return Mask.GetLength(1); } }


        public int this[int x, int y]
        {
            get { return Mask[x, y]; }
            set { Mask[x, y] = value; }
        }

        public BlobMask(int width, int height)
        {
            Mask = new int[width, height];
            RegionLabels = new List<RegionLabel>();
        }
        public BlobMask(Size size)
        {
            Mask = new int[size.Width, size.Height];
            RegionLabels = new List<RegionLabel>();
        }

        public RegionLabel AddNewRegionLabel(int x, int y)
        {
            RegionLabel rl = new RegionLabel(RegionLabels.Count + 1, x, y);
            RegionLabels.Add(rl);
            return rl;
        }
        public RegionLabel AddNewRegionLabel(Point point)
        {
            RegionLabel rl = new RegionLabel(RegionLabels.Count + 1, point);
            RegionLabels.Add(rl);
            return rl;
        }


        public BlobsCollection GetBlobsFromRegionLabels()
        {
            BlobsCollection blobs = new BlobsCollection();

            foreach (RegionLabel regionLabel in RegionLabels)
                blobs.Add(regionLabel.GetBlob(Mask));

            return blobs;
        }
        public BlobsDataset GetBlobsDataset(FastBmp original)
        {
            BlobsCollection blobs = GetBlobsFromRegionLabels();
            return new BlobsDataset(original, this, blobs);
        }
    }
}
