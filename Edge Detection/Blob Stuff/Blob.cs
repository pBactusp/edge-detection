﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edge_Detection.Blob_Stuff
{
    public class Blob
    {
        public Contour OuterContour;
        public List<Contour> InnerContours;

        public Region Region;

        public BoundingBox BoundingBox { get { return Region.BoundingBox; } }

        public Blob(Region region)
        {
            Region = region;
        }

        


        public static void RenderToMask(Blob blob, int[,] mask)
        {
            foreach (RLCoord rlc in blob.Region.RunLengthCoordinates)
                for (int i = 0; i < rlc.Length; i++)
                    mask[rlc.Column + i, rlc.Row] = blob.Region.Label.Index;
        }
    }
}
