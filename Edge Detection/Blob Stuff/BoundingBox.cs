﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace Edge_Detection.Blob_Stuff
{
    public class BoundingBox
    {
        public int Left { get; private set; }
        public int Right { get; private set; }
        public int Top { get; private set; }
        public int Bottom { get; private set; }
        public Point Center { get; private set; }

        public int Width { get { return Right - Left; } }
        public int Height { get { return Bottom - Top; } }



        public BoundingBox(int left, int right, int top, int bottom)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = bottom;

            Center = GetCenter(this);
        }
        public BoundingBox(Rectangle rect)
        {
            Left = rect.Left;
            Right = rect.Right;
            Top = rect.Top;
            Bottom = rect.Bottom;

            Center = GetCenter(this);
        }
        public BoundingBox(BoundingBox boundingBox)
        {
            Left = boundingBox.Left;
            Right = boundingBox.Right;
            Top = boundingBox.Top;
            Bottom = boundingBox.Bottom;

            Center = new Point(boundingBox.Center.X, boundingBox.Center.Y);
        }

        private static Point GetCenter(BoundingBox boundingBox)
        {
            return new Point(boundingBox.Left + boundingBox.Width / 2, boundingBox.Top + boundingBox.Height / 2);
        }

        public void AddPoint(int x, int y)
        {
            Left = Math.Min(Left, x);
            Right = Math.Max(Right, x);
            Top = Math.Min(Top, y);
            Bottom = Math.Max(Bottom, y);

            Center = GetCenter(this);
        }
        public void AddPoint(Point point)
        {
            Left = Math.Min(Left, point.X);
            Right = Math.Max(Right, point.X);
            Top = Math.Min(Top, point.Y);
            Bottom = Math.Max(Bottom, point.Y);

            Center = GetCenter(this);
        }

        public void AddArea(BoundingBox boundingBox)
        {
            Left = Math.Min(Left, boundingBox.Left);
            Right = Math.Max(Right, boundingBox.Right);
            Top = Math.Min(Top, boundingBox.Top);
            Bottom = Math.Max(Bottom, boundingBox.Bottom);

            Center = GetCenter(this);
        }
        public void AddAreas(IEnumerable<BoundingBox> boundingBoxs)
        {
            foreach (BoundingBox boundingBox in boundingBoxs)
            {
                Left = Math.Min(Left, boundingBox.Left);
                Right = Math.Max(Right, boundingBox.Right);
                Top = Math.Min(Top, boundingBox.Top);
                Bottom = Math.Max(Bottom, boundingBox.Bottom);
            }

            Center = GetCenter(this);
        }


    }
}
