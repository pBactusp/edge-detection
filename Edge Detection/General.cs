﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Edge_Detection
{
    public static class General
    {
        public static void SetPanelDoubleBuffered(Panel panel, bool doubleBuffered)
        {
            typeof(Panel).InvokeMember("DoubleBuffered",
                System.Reflection.BindingFlags.SetProperty
                | System.Reflection.BindingFlags.Instance
                | System.Reflection.BindingFlags.NonPublic,
                null, panel, new object[] { doubleBuffered });
        }
    }
}
