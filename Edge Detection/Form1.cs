﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using Edge_Detection.Blob_Stuff;
using Edge_Detection.FastBitmaps_Stuff;

namespace Edge_Detection
{
    public partial class Form1 : Form
    {
        private EdgeDataset currentED;
        private bool changeBar = true;

        public Form1()
        {
            InitializeComponent();

            General.SetPanelDoubleBuffered(outputPA, true);
        }


        private void Render()
        {
            outputPA.BackgroundImage = currentED.Render((float)thresholdBar.Value / 10000f, showOriginalImageCHEBO.Checked, useThinEdgesCHEBO.Checked, true);
        }

        private void newImageBU_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            currentED = new EdgeDataset(new Bitmap(ofd.FileName), compressCHEBO.Checked) { DefaultEdgeColor = Color.Red };

            thresholdNUPDO.Enabled = true;
            thresholdBar.Enabled = true;
            showOriginalImageCHEBO.Enabled = true;
            useThinEdgesCHEBO.Enabled = true;
            compressCHEBO.Enabled = true;
            generateBlobsToolStripMenuItem.Enabled = true;

            if (outputPA.BackgroundImage != null)
                outputPA.BackgroundImage.Dispose();

            Render();
        }

        private void thresholdBar_MouseUp(object sender, MouseEventArgs e)
        {
            changeBar = false;
            thresholdNUPDO.Value = thresholdBar.Value;

            outputPA.BackgroundImage.Dispose();
            Render();
        }

        private void thresholdNUPDO_ValueChanged(object sender, EventArgs e)
        {
            if (changeBar)
                thresholdBar.Value = (int)thresholdNUPDO.Value;

            changeBar = true;

            outputPA.BackgroundImage.Dispose();
            Render();
        }

        private void showOriginalImageCHEBO_CheckedChanged(object sender, EventArgs e)
        {
            outputPA.BackgroundImage.Dispose();
            Render();
        }
        private void useThinEdgesCHEBO_CheckedChanged(object sender, EventArgs e)
        {
            outputPA.BackgroundImage.Dispose();
            Render();
        }
        private void compressCHEBO_CheckedChanged(object sender, EventArgs e)
        {
            currentED.RecalculateEdges(compressCHEBO.Checked);
            outputPA.BackgroundImage.Dispose();
            Render();
        }

        private void generateBlobsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            float maxAngleDiff = (float)Math.PI / 8f;
            BlobsDataset blobsDataset = currentED.GetBlobsDatasetForThreshold((float)thresholdBar.Value / 10000f, maxAngleDiff, useThinEdgesCHEBO.Checked);
            blobsDataset.DefaultBlobColor = Color.Red;

            BlobsWindow blobsWindow = new BlobsWindow(blobsDataset);
            blobsWindow.Show();
        }

        private void thresholdBar_Scroll(object sender, EventArgs e)
        {

        }

        private void outputPA_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
