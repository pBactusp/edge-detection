﻿namespace Edge_Detection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newImageBU = new System.Windows.Forms.Button();
            this.outputPA = new System.Windows.Forms.Panel();
            this.thresholdBar = new System.Windows.Forms.TrackBar();
            this.thresholdNUPDO = new System.Windows.Forms.NumericUpDown();
            this.showOriginalImageCHEBO = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateBlobsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useThinEdgesCHEBO = new System.Windows.Forms.CheckBox();
            this.compressCHEBO = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdNUPDO)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // newImageBU
            // 
            this.newImageBU.Location = new System.Drawing.Point(17, 28);
            this.newImageBU.Margin = new System.Windows.Forms.Padding(4);
            this.newImageBU.Name = "newImageBU";
            this.newImageBU.Size = new System.Drawing.Size(143, 56);
            this.newImageBU.TabIndex = 0;
            this.newImageBU.Text = "New Image";
            this.newImageBU.UseVisualStyleBackColor = true;
            this.newImageBU.Click += new System.EventHandler(this.newImageBU_Click);
            // 
            // outputPA
            // 
            this.outputPA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputPA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.outputPA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outputPA.Location = new System.Drawing.Point(17, 92);
            this.outputPA.Margin = new System.Windows.Forms.Padding(4);
            this.outputPA.Name = "outputPA";
            this.outputPA.Size = new System.Drawing.Size(871, 447);
            this.outputPA.TabIndex = 1;
            this.outputPA.Paint += new System.Windows.Forms.PaintEventHandler(this.outputPA_Paint);
            // 
            // thresholdBar
            // 
            this.thresholdBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thresholdBar.Enabled = false;
            this.thresholdBar.Location = new System.Drawing.Point(168, 28);
            this.thresholdBar.Margin = new System.Windows.Forms.Padding(4);
            this.thresholdBar.Maximum = 10000;
            this.thresholdBar.Name = "thresholdBar";
            this.thresholdBar.Size = new System.Drawing.Size(720, 56);
            this.thresholdBar.TabIndex = 2;
            this.thresholdBar.Value = 6000;
            this.thresholdBar.Scroll += new System.EventHandler(this.thresholdBar_Scroll);
            this.thresholdBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.thresholdBar_MouseUp);
            // 
            // thresholdNUPDO
            // 
            this.thresholdNUPDO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.thresholdNUPDO.Enabled = false;
            this.thresholdNUPDO.Location = new System.Drawing.Point(895, 46);
            this.thresholdNUPDO.Margin = new System.Windows.Forms.Padding(4);
            this.thresholdNUPDO.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.thresholdNUPDO.Name = "thresholdNUPDO";
            this.thresholdNUPDO.Size = new System.Drawing.Size(159, 22);
            this.thresholdNUPDO.TabIndex = 3;
            this.thresholdNUPDO.Value = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            this.thresholdNUPDO.ValueChanged += new System.EventHandler(this.thresholdNUPDO_ValueChanged);
            // 
            // showOriginalImageCHEBO
            // 
            this.showOriginalImageCHEBO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showOriginalImageCHEBO.AutoSize = true;
            this.showOriginalImageCHEBO.Checked = true;
            this.showOriginalImageCHEBO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showOriginalImageCHEBO.Enabled = false;
            this.showOriginalImageCHEBO.Location = new System.Drawing.Point(895, 92);
            this.showOriginalImageCHEBO.Name = "showOriginalImageCHEBO";
            this.showOriginalImageCHEBO.Size = new System.Drawing.Size(159, 21);
            this.showOriginalImageCHEBO.TabIndex = 4;
            this.showOriginalImageCHEBO.Text = "Show Original Image";
            this.showOriginalImageCHEBO.UseVisualStyleBackColor = true;
            this.showOriginalImageCHEBO.CheckedChanged += new System.EventHandler(this.showOriginalImageCHEBO_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1067, 30);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 26);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateBlobsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(49, 26);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // generateBlobsToolStripMenuItem
            // 
            this.generateBlobsToolStripMenuItem.Enabled = false;
            this.generateBlobsToolStripMenuItem.Name = "generateBlobsToolStripMenuItem";
            this.generateBlobsToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
            this.generateBlobsToolStripMenuItem.Text = "Generate Blobs";
            this.generateBlobsToolStripMenuItem.Click += new System.EventHandler(this.generateBlobsToolStripMenuItem_Click);
            // 
            // useThinEdgesCHEBO
            // 
            this.useThinEdgesCHEBO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.useThinEdgesCHEBO.AutoSize = true;
            this.useThinEdgesCHEBO.Checked = true;
            this.useThinEdgesCHEBO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useThinEdgesCHEBO.Enabled = false;
            this.useThinEdgesCHEBO.Location = new System.Drawing.Point(895, 119);
            this.useThinEdgesCHEBO.Name = "useThinEdgesCHEBO";
            this.useThinEdgesCHEBO.Size = new System.Drawing.Size(131, 21);
            this.useThinEdgesCHEBO.TabIndex = 6;
            this.useThinEdgesCHEBO.Text = "Use Thin Edges";
            this.useThinEdgesCHEBO.UseVisualStyleBackColor = true;
            this.useThinEdgesCHEBO.CheckedChanged += new System.EventHandler(this.useThinEdgesCHEBO_CheckedChanged);
            // 
            // compressCHEBO
            // 
            this.compressCHEBO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.compressCHEBO.AutoSize = true;
            this.compressCHEBO.Checked = true;
            this.compressCHEBO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.compressCHEBO.Enabled = false;
            this.compressCHEBO.Location = new System.Drawing.Point(895, 146);
            this.compressCHEBO.Name = "compressCHEBO";
            this.compressCHEBO.Size = new System.Drawing.Size(93, 21);
            this.compressCHEBO.TabIndex = 7;
            this.compressCHEBO.Text = "Compress";
            this.compressCHEBO.UseVisualStyleBackColor = true;
            this.compressCHEBO.CheckedChanged += new System.EventHandler(this.compressCHEBO_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.compressCHEBO);
            this.Controls.Add(this.useThinEdgesCHEBO);
            this.Controls.Add(this.showOriginalImageCHEBO);
            this.Controls.Add(this.thresholdNUPDO);
            this.Controls.Add(this.thresholdBar);
            this.Controls.Add(this.outputPA);
            this.Controls.Add(this.newImageBU);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.thresholdBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdNUPDO)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button newImageBU;
        private System.Windows.Forms.Panel outputPA;
        private System.Windows.Forms.TrackBar thresholdBar;
        private System.Windows.Forms.NumericUpDown thresholdNUPDO;
        private System.Windows.Forms.CheckBox showOriginalImageCHEBO;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateBlobsToolStripMenuItem;
        private System.Windows.Forms.CheckBox useThinEdgesCHEBO;
        private System.Windows.Forms.CheckBox compressCHEBO;
    }
}

