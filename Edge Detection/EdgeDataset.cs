﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using Edge_Detection.Blob_Stuff;
using Edge_Detection.FastBitmaps_Stuff;
using DCT;

namespace Edge_Detection
{
    public class EdgeDataset
    {
        private static int[,] sobelOp;

        static EdgeDataset()
        {
            sobelOp = new int[3, 3]
            {
                {1, 0, -1 },
                {1, 0, -1 },
                {1, 0, -1 }
            };
        }

        private FastBmp original;

        public float[,] gsOriginal;
        public float[,] hEdges;
        public float[,] vEdges;
        public float[,] edges;
        public float[,] edgeAngle;
        public float[,] thinEdges;
        public float[,] extendedEdges;


        public EdgeDataset(Bitmap image, bool compressGS) : this(new FastBmp(image), compressGS) { }
        public EdgeDataset(FastBmp image, bool compressGS)
        {
            DefaultEdgeColor = Color.Black;

            original = image;
            Width = original.Width;
            Height = original.Height;

            RecalculateEdges(compressGS);
        }


        public int Width { get; }
        public int Height { get; }
        public Color DefaultEdgeColor { get; set; }


        public void RecalculateEdges(bool compress)
        {
            hEdges = new float[Width, Height];
            vEdges = new float[Width, Height];
            edges = new float[Width, Height];
            edgeAngle = new float[Width, Height];
            thinEdges = new float[Width, Height];
            extendedEdges = new float[Width, Height];
            gsOriginal = new float[Width, Height];

            GetGS(compress);
            DetectEdges();
            GetThinEdges();
        }

        private void GetGS(bool compress)
        {
            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    gsOriginal[x, y] = ColorToGS(original[x, y]);

            if (compress)
            {
                Transformer t = new Transformer();
                gsOriginal = t.Transform_IDCT(t.Transform_DCT(gsOriginal, 8), 8);
            }
        }

        private float ColorToGS(FBPixel fbPixel)
        {
            float tempCol = fbPixel.R * 0.21f + fbPixel.G * 0.71f + fbPixel.B * 0.071f;
            return tempCol / 255f;
        }

        private void DetectEdges()
        {
            int xTag, yTag;
            float hEdge, vEdge;

            for (int y = 1; y < Height - 1; y++)
                for (int x = 1; x < Width - 1; x++)
                {
                    xTag = x - 1;
                    yTag = y - 1;

                    hEdge = 0;
                    vEdge = 0;

                    for (int i = 0; i < sobelOp.GetLength(1); i++)
                        for (int g = 0; g < sobelOp.GetLength(0); g++)
                        {
                            hEdge += gsOriginal[xTag + g, yTag + i] * sobelOp[g, i] / 3;
                            vEdge += gsOriginal[xTag + g, yTag + i] * sobelOp[i, g] / 3;
                        }

                    hEdges[x, y] = hEdge;
                    vEdges[x, y] = vEdge;

                    edges[x, y] = (float)Math.Sqrt(hEdge * hEdge + vEdge * vEdge);
                    edgeAngle[x, y] = (float)Math.Atan2(vEdge, hEdge);
                }
        }


        public Bitmap Render(float threshold, bool withOriginal, bool useThinEdges, bool withGradient)
        {
            FastBmp fbmp;
            float[,] currentEdges;

            if (withOriginal)
                fbmp = new FastBmp(gsOriginal);
            else
                fbmp = new FastBmp(Width, Height, Color.FromArgb(0, 0, 0, 0));

            if (useThinEdges)
                currentEdges = thinEdges;
            else
                currentEdges = edges;

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    if (currentEdges[x, y] >= threshold)
                    {
                        if (withGradient)
                            fbmp[x, y].Set(255, (byte)(currentEdges[x, y] * DefaultEdgeColor.R), (byte)(currentEdges[x, y] * DefaultEdgeColor.G), (byte)(255 - (byte)(currentEdges[x, y] * DefaultEdgeColor.R)));
                        else
                            fbmp[x, y].SetColor(DefaultEdgeColor);
                    }

            return fbmp.RenderToBitmap();
        }


        public BlobsDataset GetBlobsDatasetForThreshold(float threshold, float maxAngleDiff, bool useThinEdges)
        {
            return GetBlobMask(threshold, maxAngleDiff, useThinEdges).GetBlobsDataset(original);
        }

        public BlobMask GetBlobMask(float threshold, float maxAngleDiff, bool useThinEdges)
        {
            BlobMask bMask = new BlobMask(Width, Height);
            float[,] currentEdges;

            if (useThinEdges)
                currentEdges = thinEdges;
            else
                currentEdges = edges;

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    LookForNewRegion(x, y, threshold, maxAngleDiff, bMask, currentEdges);

            return bMask;
        }

        private void LookForNewRegion(int x, int y, float threshold, float maxAngleDiff, BlobMask bMask, float[,] currentEdges)
        {

            if (CoordinateCanBeInBlob(x, y, threshold, edgeAngle[x, y], maxAngleDiff, bMask, currentEdges))
            {
                Queue<Point> pointsQueue = new Queue<Point>();
                Queue<float> prevAnglesQueue = new Queue<float>();
                RegionLabel currentRL;

                currentRL = bMask.AddNewRegionLabel(x, y);
                bMask[x, y] = currentRL.Index;

                if (x > 0)
                {
                    pointsQueue.Enqueue(new Point(x - 1, y));
                    prevAnglesQueue.Enqueue(edgeAngle[x, y]);

                    if (y > 0)
                    {
                        pointsQueue.Enqueue(new Point(x - 1, y - 1));
                        prevAnglesQueue.Enqueue(edgeAngle[x, y]);
                    }
                    if (y < Height - 1)
                    {
                        pointsQueue.Enqueue(new Point(x - 1, y + 1));
                        prevAnglesQueue.Enqueue(edgeAngle[x, y]);
                    }
                }
                if (x < Width - 1)
                {
                    pointsQueue.Enqueue(new Point(x + 1, y));
                    prevAnglesQueue.Enqueue(edgeAngle[x, y]);

                    if (y > 0)
                    {
                        pointsQueue.Enqueue(new Point(x + 1, y - 1));
                        prevAnglesQueue.Enqueue(edgeAngle[x, y]);
                    }
                    if (y < Height - 1)
                    {
                        pointsQueue.Enqueue(new Point(x + 1, y + 1));
                        prevAnglesQueue.Enqueue(edgeAngle[x, y]);
                    }
                }
                if (y > 0)
                {
                    pointsQueue.Enqueue(new Point(x, y - 1));
                    prevAnglesQueue.Enqueue(edgeAngle[x, y]);
                }
                if (y < Height - 1)
                {
                    pointsQueue.Enqueue(new Point(x, y + 1));
                    prevAnglesQueue.Enqueue(edgeAngle[x, y]);
                }

                while (pointsQueue.Count > 0)
                {
                    Point currentPoint = pointsQueue.Dequeue();
                    float prevAngle = prevAnglesQueue.Dequeue();
                    if (CoordinateCanBeInBlob(currentPoint.X, currentPoint.Y, threshold, prevAngle, maxAngleDiff, bMask, currentEdges))
                    {
                        currentRL.AddPoint(currentPoint);
                        bMask[currentPoint.X, currentPoint.Y] = currentRL.Index;

                        if (currentPoint.X > 0)
                        {
                            pointsQueue.Enqueue(new Point(currentPoint.X - 1, currentPoint.Y));
                            prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);

                            if (currentPoint.Y > 0)
                            {
                                pointsQueue.Enqueue(new Point(currentPoint.X - 1, currentPoint.Y - 1));
                                prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);
                            }
                            if (currentPoint.Y < Height - 1)
                            {
                                pointsQueue.Enqueue(new Point(currentPoint.X - 1, currentPoint.Y + 1));
                                prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);
                            }
                        }
                        if (currentPoint.X < Width - 1)
                        {
                            pointsQueue.Enqueue(new Point(currentPoint.X + 1, currentPoint.Y));
                            prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);

                            if (currentPoint.Y > 0)
                            {
                                pointsQueue.Enqueue(new Point(currentPoint.X + 1, currentPoint.Y - 1));
                                prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);
                            }
                            if (currentPoint.Y < Height - 1)
                            {
                                pointsQueue.Enqueue(new Point(currentPoint.X + 1, currentPoint.Y + 1));
                                prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);
                            }
                        }
                        if (currentPoint.Y > 0)
                        {
                            pointsQueue.Enqueue(new Point(currentPoint.X, currentPoint.Y - 1));
                            prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);
                        }
                        if (currentPoint.Y < Height - 1)
                        {
                            pointsQueue.Enqueue(new Point(currentPoint.X, currentPoint.Y + 1));
                            prevAnglesQueue.Enqueue(edgeAngle[currentPoint.X, currentPoint.Y]);
                        }
                    }

                }


            }
        }
        private bool CoordinateCanBeInBlob(int x, int y, float threshold, float previousAngle, float maxAngleDiff, BlobMask bMask, float[,] currentEdges)
        {

            return bMask[x, y] == 0 && currentEdges[x, y] >= threshold && AnglesAreSimlar(edgeAngle[x, y], previousAngle, maxAngleDiff);
        }


        private void GetThinEdges()
        {
            float maxAngleDiff = (float)Math.PI / 8f;
            float halfPi = (float)Math.PI / 2f;

            Point current, delta;
            Point[] adjPoints = new Point[2];

            bool strongerThanAdj;

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                {
                    strongerThanAdj = true;

                    current = new Point(x, y);
                    delta = GetDeltaFromAngle(edgeAngle[x, y] + halfPi);
                    adjPoints[0] = new Point(x + delta.X, y + delta.Y);
                    adjPoints[1] = new Point(x - delta.X, y - delta.Y);

                    for (int i = 0; strongerThanAdj && i < adjPoints.Length; i++)
                        if (PointInBounds(Width, Height, adjPoints[i]))
                            if (AnglesAreSimlar(current, adjPoints[i], maxAngleDiff))
                                if (edges[adjPoints[i].X, adjPoints[i].Y] > edges[x, y])
                                    strongerThanAdj = false;

                    if (strongerThanAdj)
                        thinEdges[x, y] = edges[x, y];
                }
        }

        private bool PointInBounds(int width, int height, Point point)
        {
            return point.X > 0 && point.X < width && point.Y > 0 && point.Y < height;
        }

        private bool AnglesAreSimlar(float angle1, float angle2, float maxAngleDiff)
        {
            return Math.Pow(angle1 - angle2, 2) <= maxAngleDiff * maxAngleDiff;
        }

        private bool AnglesAreSimlar(Point p1, Point p2, float maxAngleDiff)
        {
            return Math.Pow(edgeAngle[p1.X, p1.Y] - edgeAngle[p2.X, p2.Y], 2) <= maxAngleDiff * maxAngleDiff;
        }

        public static Point GetDeltaFromAngle(float angle)
        {
            return new Point((int)Math.Round(Math.Cos(angle)), (int)Math.Round(Math.Sin(angle)));
        }
    }
}
