﻿using System;

namespace DCT
{
    public class Transformer
    {
        private static float[,] bias;

        static Transformer()
        {
            bias = new float[8, 8]
            {
                {16, 11, 10, 16, 24, 40, 51, 61 },
                {12, 12, 14, 19, 26, 58, 60, 55 },
                {14, 13, 16, 24, 40, 57, 69, 56 },
                {14, 17, 22, 29, 51, 87, 80, 62 },
                {18, 22, 37, 56, 68, 109, 103, 77 },
                {24, 35, 55, 64, 81, 104, 113, 92 },
                {49, 64, 78, 87, 103, 121, 120, 101 },
                {72, 92, 95, 98, 112, 100, 103, 99 }
            };

            //bias = new float[8, 8]
            //{
            //    {1, 1, 1, 0, 0, 0, 0, 0 },
            //    {1, 1, 0, 0, 0, 0, 0, 0 },
            //    {1, 0, 0, 0, 0, 0, 0, 0 },
            //    {0, 0, 0, 0, 0, 0, 0, 0 },
            //    {0, 0, 0, 0, 0, 0, 0, 0 },
            //    {0, 0, 0, 0, 0, 0, 0, 0 },
            //    {0, 0, 0, 0, 0, 0, 0, 0 },
            //    {0, 0, 0, 0, 0, 0, 0, 0 }
            //};
        }


        public float[,] Transform_DCT(float[,] source, int N)
        {
            float[,] ret = new float[source.GetLength(0), source.GetLength(1)];

            for (int y = 0; y < source.GetLength(1); y += N)
            {
                for (int x = 0; x < source.GetLength(0); x += N)
                {
                    float[,] temp = ExtractValues(source, x, y, N, N);
                    InsertValues(DCT2_Block(temp), ret, x, y);
                }
            }

            return ret;
        }
        public float[,] Transform_IDCT(float[,] source, int N)
        {
            float[,] ret = new float[source.GetLength(0), source.GetLength(1)];

            for (int y = 0; y < source.GetLength(1); y += N)
            {
                for (int x = 0; x < source.GetLength(0); x += N)
                {
                    float[,] temp = ExtractValues(source, x, y, N, N);

                    InsertValues(IDCT2_Block(temp), ret, x, y);
                }
            }

            return ret;
        }



        private float[,] DCT2_Block(float[,] source)
        {
            float[,] ret = new float[source.GetLength(0), source.GetLength(1)];

            for (int y = 0; y < source.GetLength(1); y++)
            {
                for (int x = 0; x < source.GetLength(0); x++)
                {
                    ret[x, y] = DCT2_Single(x, y, source);
                }
            }

            return ret;
        }
        private float DCT2_Single(int x, int y, float[,] source)
        {
            double c = 0;

            // Constants
            int width = source.GetLength(0);
            int height = source.GetLength(1);

            double _nConst = x * Math.PI / width;
            double _mConst = y * Math.PI / height;

            for (int m = 0; m < height; m++)
            {
                double mCos = Math.Cos((m + 0.5) * _mConst);
                
                for (int n = 0; n < width; n++)
                {
                    //float xCo = Math.Cos(l * _xConst);
                    //float yCo = Math.Cos(k * _yConst);
                    //c += xCo * yCo;

                    c += (source[n, m]) * mCos * Math.Cos((n + 0.5) * _nConst);
                }
            }

            double a = (x == 0 ? 1.0 / Math.Sqrt(width) : Math.Sqrt(2.0 / width)) * (y == 0 ? 1.0 / Math.Sqrt(height) : Math.Sqrt(2.0 / height));

            c *= a / bias[x, y];

            return (float)c;
        }

        private float[,] IDCT2_Block(float[,] source)
        {
            float[,] ret = new float[source.GetLength(0), source.GetLength(1)];

            for (int y = 0; y < source.GetLength(1); y++)
            {
                for (int x = 0; x < source.GetLength(0); x++)
                {
                    ret[x, y] = IDCT2_Single(x, y, source);
                }
            }

            return ret;
        }
        private float IDCT2_Single(int x, int y, float[,] source)
        {
            double c = 0;

            // Constants
            int width = source.GetLength(0);
            int height = source.GetLength(1);

            double _nConst = (x + 0.5) * Math.PI / width;
            double _mConst = (y + 0.5) * Math.PI / height;

            for (int m = 0; m < height; m++)
            {
                double mCos = Math.Cos(m * _mConst);
                double mA = m == 0 ? 1.0 / Math.Sqrt(height) : Math.Sqrt(2.0 / height);

                for (int n = 0; n < width; n++)
                {
                    double nA = n == 0 ? 1.0 / Math.Sqrt(width) : Math.Sqrt(2.0 / width);

                    //float xCo = Math.Cos(l * _xConst);
                    //float yCo = Math.Cos(k * _yConst);
                    //c += xCo * yCo;

                    c += nA * mA * source[n, m] * mCos * Math.Cos(n * _nConst);
                }
            }

            return (float)c;
        }


        private float[,] ExtractValues(float[,] from, int x, int y, int width, int height)
        {
            if (width + x >= from.GetLength(0))
                width = from.GetLength(0) - x;
            if (height + y >= from.GetLength(1))
                height = from.GetLength(1) - y;

            float[,] ret = new float[width, height];

            for (int i = 0; i < height; i++)
                for (int g = 0; g < width; g++)
                    ret[g, i] = from[x + g, y + i];

            return ret;
        }
        private void InsertValues(float[,] from, float[,] to, int x, int y)
        {
            for (int i = 0; i < from.GetLength(1); i++)
                for (int g = 0; g < from.GetLength(0); g++)
                    to[x + g, y + i] = from[g, i];
        }

    }
}
